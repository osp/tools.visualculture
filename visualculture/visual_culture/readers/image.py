"""
visual_culture.readers.image

"""

from visual_culture.readers import reader
from PIL import Image
import StringIO


@reader(r'image/(gif|png|jpeg|jpg|tiff)')
class VC_Image(object):
    """
    Handle any kind of images
    """
    def __init__(self):
        pass
    
    def scale_image(self, image, options):
        w = image.width * 1.0
        h = image.height * 1.0

        width = 0
        height = 0
        if 'width' in options:
            width = int(options['width'])
            ratio = w / width
            height = h / ratio
        if 'height' in options:
            height = int(options['height'])
            ratio = h / height
            width = w / ratio
                
        if height == 0 and width == 0:
            width = height = 200

        resized = image.resize((int(width),int(height)), Image.ANTIALIAS)
        return resized
    
    def read_blob(self, blob_info, options):
        blob_data = self.get_blob_data(blob_info)
        buf_in = StringIO.StringIO(blob_data)
        image = Image.open(buf_in)
        simage = self.scale_image(image, options)
        buf_out = StringIO.StringIO()
        simage.save(buf_out, 'PNG') # check options there
        data = buf_out.getvalue()
        buf_in.close()
        buf_out.close()
        return {'data':data, 'mime': 'image/png'}
